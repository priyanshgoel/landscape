/**
 * @ Point.java
 *  	
 * 
 */
package com.thoughtspot.hiring.models;

/**
 * The <code>Point</code> represents {description}
 * <p>
 * <li>{Enclosing Methods}</li> {short description}
 *
 * Created at Jun 29, 2017 08:40:06 PM
 * 
 * @author pgoel (last updated by $Author$)
 * @version $Revision$ $Date$
 * 
 */
public class Point {

	private int x;
	private int y;

	/**
	 * The constructor for <code>Point</code> having following parameters
	 * 
	 * @param x
	 * @param y
	 * 
	 */
	public Point(int x, int y) {
		super();
		this.x = x;
		this.y = y;

	}

	/**
	 * @return the x
	 */
	public int getX() {
		return this.x;
	}

	/**
	 * @param x
	 *            the x to set
	 */
	public void setX(int x) {
		this.x = x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return this.y;
	}

	/**
	 * @param y
	 *            the y to set
	 */
	public void setY(int y) {
		this.y = y;
	}
	
	
	@Override
	public boolean equals(Object o) {
		if(!(o instanceof Point)) {
			return false;
		}
		
		Point p = (Point) o;
		
		if(this.getX() == p.getX() && this.getY() == p.getY())  return true;
		
		return false;
		
	}

}
