/**
 * @ InvalidDataException.java
 *  	
 * 
 */
package com.thoughtspot.hiring.exception;

/**
 * The <code>InvalidDataException</code> represents {description}
 * <p>
 * <li>{Enclosing Methods}</li> {short description}
 *
 * Created at Jun 29, 2017 11:46:19 PM
 * 
 * @author pgoel (last updated by $Author$)
 * @version $Revision$ $Date$
 * 
 */
public class InvalidDataException extends RuntimeException {

	/**
	 * long
	 */
	private static final long serialVersionUID = 1L;
	private String description;

	/**
	 * The constructor for <code>InvalidDataException</code> having following
	 * parameters
	 * 
	 * @param description
	 */
	public InvalidDataException(String description) {
		super();
		this.description = description;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description
	 *            the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

}
