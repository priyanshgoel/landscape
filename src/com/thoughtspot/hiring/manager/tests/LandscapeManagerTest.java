/**
 * @ LandscapeManagerTest.java
 *  	
 * 
 */
package com.thoughtspot.hiring.manager.tests;

import java.util.List;

import org.junit.Test;
import org.junit.internal.runners.JUnit4ClassRunner;
import org.junit.runner.RunWith;

import com.thoughtspot.hiring.exception.InvalidDataException;
import com.thoughtspot.hiring.manager.LandscapeManager;
import com.thoughtspot.hiring.models.Point;

import junit.framework.Assert;

/**
 * The <code>LandscapeManagerTest</code> represents {description}
 * <p>
 * <li>{Enclosing Methods}</li> {short description}
 *
 * Created at Jun 29, 2017 10:37:37 PM
 * 
 * @author pgoel (last updated by $Author$)
 * @version $Revision$ $Date$
 * 
 */
@SuppressWarnings("deprecation")
@RunWith(JUnit4ClassRunner.class)
public class LandscapeManagerTest {

	// ************************************Get All Lakes Test Cases ************************

	// Success Case1 - General case
	@Test
	public void testGetAllLakes_Happy1() {

		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		int threshold = 6;

		List<List<Point>> allLakes = landscapeManager.getAllLakes(grid, row, col, threshold);

		Assert.assertEquals(2,allLakes.size());

	}
	
	//Success Case 2 - There is a lake with only 1 element. 
	@Test
	public void testGetAllLakes_Happy2() {

		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 3;
		int col = 3;

		int grid[][] = new int[][] { { 4, 5, 2}, { 3, 6, 6} , {4,6,3} };

		int threshold = 3;

		List<List<Point>> allLakes = landscapeManager.getAllLakes(grid, row, col, threshold);

		Assert.assertEquals(1,allLakes.size());
		Assert.assertEquals(1, allLakes.get(0).size());

	}
	
	//When there are no  possible lakes.
	@Test
	public void testGetAllLakesWithNoLake() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		int threshold = 0;

		List<List<Point>> allLakes = landscapeManager.getAllLakes(grid, row, col, threshold);

		Assert.assertEquals(0,allLakes.size());
	}
	
	//When the whole grid is a lake. 
	@Test
	public void testGetAllLakesWithFullGridAsLake() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		int threshold = 10;

		List<List<Point>> allLakes = landscapeManager.getAllLakes(grid, row, col, threshold);

		Assert.assertEquals(1,allLakes.size());
	}

	//When the grid is empty
	@Test(expected = InvalidDataException.class)
	public void testGetAllLakesWithEmptyGrid() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 0;
		int col = 0;

		int grid[][] = new int[0][0]; 

		int threshold = 10;

		 landscapeManager.getAllLakes(grid, row, col, threshold);

		
	}
	
	//When Grid has one element only
	@Test
	public void testGetAllLakesWithOneElementInGrid(){
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 1;
		int col = 1;

		int grid[][] = new int[][] { { 4} };

		int threshold = 10;

		List<List<Point>> allLakes = landscapeManager.getAllLakes(grid, row, col, threshold);

		Assert.assertEquals(1,allLakes.size());
	}
	
	
	//When Grid has one row
	@Test
	public void testGetAllLakesWithOneRowGrid() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 1;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 } };

		int threshold = 8;

		List<List<Point>> allLakes = landscapeManager.getAllLakes(grid, row, col, threshold);

		Assert.assertEquals(2,allLakes.size());
	}
	
	//When Grid has one column
	@Test
	public void testGetAllLakesWithOneColGrid() {
		
		LandscapeManager landscapeManager = new LandscapeManager();
		int row = 2;
		int col = 1;
		int grid[][] = new int[][] { {4} ,{3} };

		int threshold = 4;

		List<List<Point>> allLakes = landscapeManager.getAllLakes(grid, row, col, threshold);

		Assert.assertEquals(1,allLakes.size());
		
	}
	
	//Invalid row test case
	@Test(expected = InvalidDataException.class)
	public void testGetAllLakesWithInvalidRow() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = -3;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		int threshold = 6;

		landscapeManager.getAllLakes(grid, row, col, threshold);
	}
	
	//Invalid col test case
	@Test(expected = InvalidDataException.class)
	public void testGetAllLakesWithInvalidColumn() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = -2;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		int threshold = 6;

		landscapeManager.getAllLakes(grid, row, col, threshold);
	}
	
	//Invalid threshold test case 
	@Test(expected = InvalidDataException.class)
	public void testGetAllLakesWithInvalidThreshold() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		int threshold = -6;

		landscapeManager.getAllLakes(grid, row, col, threshold);
	}
	
	//Invalid z values test case
	@Test(expected = InvalidDataException.class)
	public void testGetAllLakesWithInvalidZValues() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, -2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		int threshold = 6;

		landscapeManager.getAllLakes(grid, row, col, threshold);
	}
	
	
	
	
	//*************************** Get Maximum Surface Area test cases ****************************************

	//Success case 1
	@Test
	public void testMaximumSurfaceArea_Happy1() {

		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		int threshold = 6;

		int surfaceArea = landscapeManager.getMaximumSurfaceArea(grid, row, col, threshold);

		Assert.assertEquals(5,surfaceArea);

	}
	
	//Success case 2 - When there is lake with only one point
	@Test
	public void testMaximumSurfaceArea_Happy2() {
	
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 3;
		int col = 3;

		int grid[][] = new int[][] { { 4, 5, 2}, { 3, 6, 6} , {4,6,3} };

		int threshold = 3;
		
		int surfaceArea = landscapeManager.getMaximumSurfaceArea(grid, row, col, threshold);

		Assert.assertEquals(1, surfaceArea);
	}
	
	//When there are no lakes.
	@Test
	public void testMaximumSurfaceAreaWithNoLakes() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		int threshold = 0;

		int surfaceArea = landscapeManager.getMaximumSurfaceArea(grid, row, col, threshold);

		Assert.assertEquals(0,surfaceArea);
	}
	
	//When the whole grid is a lake. 
	@Test
	public void testMaximumSurfaceAreaWithOneLake() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		int threshold = 10;
		
		int surfaceArea = landscapeManager.getMaximumSurfaceArea(grid, row, col, threshold);

		Assert.assertEquals(10,surfaceArea);
		

	}
	
	//When the grid is empty
	@Test(expected=InvalidDataException.class)
	public void testMaximumSurfaceAreaWithEmptyGrid() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 0;
		int col = 0;

		int grid[][] = new int[0][0]; 

		int threshold = 10;
		
		landscapeManager.getMaximumSurfaceArea(grid, row, col, threshold);

		
	}
	
	//When Grid has one element only
	@Test
	public void testMaximumSurfaceAreaWithOneElementInGrid(){
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 1;
		int col = 1;

		int grid[][] = new int[][] { { 4} };

		int threshold = 5;
		
		int surfaceArea = landscapeManager.getMaximumSurfaceArea(grid, row, col, threshold);

		Assert.assertEquals(1,surfaceArea);
	}
	
	//When the grid has one row.
	@Test
	public void testMaximumSurfaceAreaWithOneRow() {
		
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 1;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 } };

		int threshold = 8;
		
		int surfaceArea = landscapeManager.getMaximumSurfaceArea(grid, row, col, threshold);

		Assert.assertEquals(3,surfaceArea);
	}
	
	//When Grid has one column.
	@Test
	public void testMaximumSurfaceAreaWithOneCol() {
		
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 1;

		int grid[][] = new int[][] { { 4 }, {3} };

		int threshold = 4;
		
		int surfaceArea = landscapeManager.getMaximumSurfaceArea(grid, row, col, threshold);

		Assert.assertEquals(1,surfaceArea);
	}
	
	
	//Invalid row
	@Test(expected = InvalidDataException.class) 
	public void testMaximumSurfaceAreaWithInvalidRow() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = -2;
		int col = 1;

		int grid[][] = new int[][] { { 4 }, {3} };

		int threshold = 4;
		
		landscapeManager.getMaximumSurfaceArea(grid, row, col, threshold);
	}
	
	//Invalid column
	@Test(expected = InvalidDataException.class) 
	public void testMaximumSurfaceAreaWithInvalidCol() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = -1;

		int grid[][] = new int[][] { { 4 }, {3} };

		int threshold = 4;
		
		landscapeManager.getMaximumSurfaceArea(grid, row, col, threshold);
	}
	
	//Invalid Threshold
	@Test(expected = InvalidDataException.class) 
	public void testMaximumSurfaceAreaWithInvalidThreshold() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 1;

		int grid[][] = new int[][] { { 4 }, {3} };

		int threshold = -4;
		
		landscapeManager.getMaximumSurfaceArea(grid, row, col, threshold);
	}
	
	//Invalid z values
	@Test(expected = InvalidDataException.class) 
	public void testMaximumSurfaceAreaWithInvalidZValues() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 1;

		int grid[][] = new int[][] { { -4 }, {3} };

		int threshold = 4;
		
		landscapeManager.getMaximumSurfaceArea(grid, row, col, threshold);
	}
	
	
	
	
	
	//*************************** Maximum Volume Test Cases **************************************
	
	//Success 1 - General case
	@Test
	public void testMaximumVolume_Happy1() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		int threshold = 6;

		int volume = landscapeManager.getMaximumVolume(grid, row, col, threshold);

		Assert.assertEquals(16,volume);
	}
	
	//Success 2 - There is a lake with only 1 element. 
	@Test
	public void testMaximumVolume_Happy2() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 3;
		int col = 3;

		int grid[][] = new int[][] { { 4, 5, 2}, { 3, 6, 6} , {4,6,3} };

		int threshold = 3;
		
		int volume = landscapeManager.getMaximumVolume(grid, row, col, threshold);
		
		Assert.assertEquals(2,volume);
		
	}
	
	//When there are no lakes.
	@Test
	public void testMaximumVolumeWithNoLakes() {
		
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		int threshold = 0;

		int volume = landscapeManager.getMaximumVolume(grid, row, col, threshold);

		Assert.assertEquals(0,volume);
	}
	
	//When the whole grid is lake
	@Test
	public void testMaximumVolumeWithWholeGridAsLake() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		int threshold = 10;

		int volume = landscapeManager.getMaximumVolume(grid, row, col, threshold);

		Assert.assertEquals(42,volume);
	}
	
	//When the grid is empty
	@Test(expected = InvalidDataException.class)
	public void testMaximumVolumeWithEmptyGrid() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 0;
		int col = 0;

		int grid[][] = new int[0][0]; 

		int threshold = 10;

		landscapeManager.getMaximumVolume(grid, row, col, threshold);
	}
	
	//When the grid has only one element
	@Test
	public void testMaximumVolumeWithGridWithOneElement(){
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 1;
		int col = 1;

		int grid[][] = new int[][] { { 4 } };

		int threshold = 10;

		int volume = landscapeManager.getMaximumVolume(grid, row, col, threshold);

		Assert.assertEquals(4,volume);
	}
	
	//When the grid has one row
	@Test
	public void testMaximumVolumeWithOneRowGrid() {
		
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 1;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 } };

		int threshold = 8;

		int volume = landscapeManager.getMaximumVolume(grid, row, col, threshold);

		Assert.assertEquals(11,volume);
	}
	
	//When the grid has one column
	@Test
	public void testMaximumVolumeWithOneColGrid() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 1;

		int grid[][] = new int[][] { {4},{3} };

		int threshold = 4;

		int volume = landscapeManager.getMaximumVolume(grid, row, col, threshold);

		Assert.assertEquals(3,volume);
	}
	
	//Invalid row
	@Test(expected = InvalidDataException.class)
	public void testMaximumVolumeWithInvalidRow() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = -1;
		int col = 1;

		int grid[][] = new int[][] { {4},{3} };

		int threshold = 4;

		landscapeManager.getMaximumVolume(grid, row, col, threshold);
	}
	
	//Invalid column
	@Test(expected = InvalidDataException.class)
	public void testMaximumVolumeWithInvalidCol() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = -1;

		int grid[][] = new int[][] { {4},{3} };

		int threshold = 4;

		landscapeManager.getMaximumVolume(grid, row, col, threshold);
	}
	
	//Invalid Threshold
	@Test(expected = InvalidDataException.class)
	public void testMaximumVolumeWithInvalidThreshold() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 1;

		int grid[][] = new int[][] { {4},{3} };

		int threshold = -4;

		landscapeManager.getMaximumVolume(grid, row, col, threshold);
	}
	
	//Invalid z values
	@Test(expected = InvalidDataException.class)
	public void testMaximumVolumeWithInvalidZValues() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 1;

		int grid[][] = new int[][] { {4},{-3} };

		int threshold = 4;

		landscapeManager.getMaximumVolume(grid, row, col, threshold);
	}
	
	
	
	
	// ************************************* Shortest Motorable Path test cases *********************************
	
	//Success Case 1 
	@Test
	public void testGetShortestMotorablePath_Happy1() {

		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		Point source = new Point(0, 1);
		Point destination = new Point(1, 4);

		int threshold = 10;

		List<Point> path = landscapeManager.getShortestMotorablePath(grid, row, col, source, destination, threshold);

		Assert.assertEquals(5,path.size());

	}
	
	//Success Case 2 
		@Test
		public void testGetShortestMotorablePath_Happy2() {

			LandscapeManager landscapeManager = new LandscapeManager();

			int row = 2;
			int col = 5;

			int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

			Point source = new Point(1,4);
			Point destination = new Point(0,1);

			int threshold = 10;

			List<Point> path = landscapeManager.getShortestMotorablePath(grid, row, col, source, destination, threshold);

			Assert.assertEquals(5,path.size());

		}
		
	
	
	//When source and destination are same. 
	
	@Test
	public void testGetShortestMotorablePathWithSameSourceAndDest() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		Point source = new Point(0, 1);
		Point destination = new Point(0,1);

		int threshold = 10;

		List<Point> path = landscapeManager.getShortestMotorablePath(grid, row, col, source, destination, threshold);

		Assert.assertEquals(1,path.size());
	}

	//When there is no Path available
	@Test
	public void testGetShortestMotorablePathIfPathNotAvailable() {

		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		Point source = new Point(0, 1);
		Point destination = new Point(1, 4);

		int threshold = 3;

		List<Point> path = landscapeManager.getShortestMotorablePath(grid, row, col, source, destination, threshold);

		Assert.assertEquals(null,path);

	}
	
	//Empty grid
	@Test(expected = InvalidDataException.class)
	public void testGetShortestPathWithEmptyGrid(){
		
		LandscapeManager landscapeManager = new LandscapeManager();
		
		int row = 0;
		int col = 0;

		int grid[][] = new int[0][0];

		Point source = new Point(0,1);
		Point destination = new Point(1, 4);

		int threshold = 3;

		landscapeManager.getShortestMotorablePath(grid, row, col, source, destination, threshold);
	}
	
	//When grid has only one element
	@Test
	public void testGetShortestPathWithOneElementGrid() {
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 1;
		int col = 1;

		int grid[][] = new int[][] { {4} };

		Point source = new Point(0, 0);
		Point destination = new Point(0, 0);

		int threshold = 3;

		List<Point> path = landscapeManager.getShortestMotorablePath(grid, row, col, source, destination, threshold);

		Assert.assertEquals(1,path.size());
	}
	
	//One Row Grid
	@Test
	public void testGetShortestPathWithOneRow(){
		
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 1;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 } };

		Point source = new Point(0, 0);
		Point destination = new Point(0, 4);

		int threshold = 3;

		List<Point> path = landscapeManager.getShortestMotorablePath(grid, row, col, source, destination, threshold);
		Assert.assertEquals(null,path);
	}
	
	//One column grid
	@Test
	public void testGetShortestPathWithOneCol(){
		LandscapeManager landscapeManager = new LandscapeManager();

		int row = 2;
		int col = 1;

		int grid[][] = new int[][] { {4},{3} };

		Point source = new Point(0, 0);
		Point destination = new Point(1, 0);

		int threshold = 3;

		List<Point> path = landscapeManager.getShortestMotorablePath(grid, row, col, source, destination, threshold);
		Assert.assertEquals(2,path.size());
	}
	
	
	//Invalid source point
	@Test(expected = InvalidDataException.class) 
	public void testGetShortestPathWithInvalidSource(){
		
		LandscapeManager landscapeManager = new LandscapeManager();
		
		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		Point source = new Point(0, -1);
		Point destination = new Point(1, 4);

		int threshold = 3;

		landscapeManager.getShortestMotorablePath(grid, row, col, source, destination, threshold);
		
		
	}
	
	//Invalid Destination point
	@Test(expected = InvalidDataException.class) 
	public void testGetShortestPathWithInvalidDest(){
		
		LandscapeManager landscapeManager = new LandscapeManager();
		
		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		Point source = new Point(0, 1);
		Point destination = new Point(1, 10);

		int threshold = 3;

		landscapeManager.getShortestMotorablePath(grid, row, col, source, destination, threshold);
		
		
	}
	
	//Invalid row
	@Test(expected = InvalidDataException.class) 
	public void testGetShortestPathWithInvalidRow(){
		
		LandscapeManager landscapeManager = new LandscapeManager();
		
		int row = -2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		Point source = new Point(0, 1);
		Point destination = new Point(1,4);

		int threshold = 3;

		landscapeManager.getShortestMotorablePath(grid, row, col, source, destination, threshold);
		
		
	}
	
	//Invalid column
	@Test(expected = InvalidDataException.class) 
	public void testGetShortestPathWithInvalidCol(){
		
		LandscapeManager landscapeManager = new LandscapeManager();
		
		int row = 2;
		int col = -5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		Point source = new Point(0, 1);
		Point destination = new Point(1, 4);

		int threshold = 3;

		landscapeManager.getShortestMotorablePath(grid, row, col, source, destination, threshold);
		
		
	}
	
	//Invalid threshold
	@Test(expected = InvalidDataException.class) 
	public void testGetShortestPathWithInvalidThreshold(){
		
		LandscapeManager landscapeManager = new LandscapeManager();
		
		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, 2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		Point source = new Point(0, 1);
		Point destination = new Point(1, 4);

		int threshold = -3;

		landscapeManager.getShortestMotorablePath(grid, row, col, source, destination, threshold);
		
		
	}
	
	//Invalid z values
	@Test(expected = InvalidDataException.class) 
	public void testGetShortestPathWithInvalidZValues(){
		
		LandscapeManager landscapeManager = new LandscapeManager();
		
		int row = 2;
		int col = 5;

		int grid[][] = new int[][] { { 4, 5, -2, 8, 1 }, { 3, 2, 6, 9, 2 } };

		Point source = new Point(0, 1);
		Point destination = new Point(1, 4);

		int threshold = 3;

		landscapeManager.getShortestMotorablePath(grid, row, col, source, destination, threshold);
		
		
	}
	
	
	

}
