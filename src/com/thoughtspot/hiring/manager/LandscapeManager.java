/**
 * @ LandscapeManager.java
 *  	
 *
 */
package com.thoughtspot.hiring.manager;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import com.thoughtspot.hiring.exception.InvalidDataException;
import com.thoughtspot.hiring.models.Point;

/**
 * The <code>LandscapeManager</code> represents the manager class for landscape.
 * It has all the methods needed to be supported on landcape.
 *
 *
 * Created at Jun 29, 2017 08:42:41 PM
 * 
 * @author pgoel (last updated by $Author$)
 * @version $Revision$ $Date$
 * 
 */
public class LandscapeManager {

	/**
	 * 
	 * The method <code>getAllLakes</code> gives all the distinct lakes for a
	 * grid. A lake is a set of points whose "z" values are all below a given
	 * threshold and any two points in the lake are connected by at least one
	 * path where each point in the path is also below threshold.
	 * 
	 * @param grid
	 *            - the "z" values of all points in a grid.
	 * @param row
	 *            - the number of rows in grid
	 * @param col
	 *            - the number of columns in grid
	 * @param threshold
	 *            - the threshold value
	 * @return List<List<Point>> - the list of all lakes. A lake is represented
	 *         as a list of points.
	 * @see
	 */
	public List<List<Point>> getAllLakes(int grid[][], int row, int col, int threshold) {

		validateParams(grid, row, col, threshold);

		// visited matrix to track all the points that are visited in dfs.
		boolean visited[][] = new boolean[row][col];

		List<List<Point>> allLakes = new ArrayList<>();

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				// Check if this point is eligible to be part of a lake and is
				// not visited
				if (!visited[i][j] && grid[i][j] < threshold) {
					List<Point> lake = new ArrayList<>();
					dfsVisit(grid, row, col, threshold, visited, i, j, lake);
					allLakes.add(lake);
				}
			}
		}

		return allLakes;

	}

	/**
	 * 
	 * The method <code>getMaximumSurfaceArea</code> returns the maximum surface
	 * area of the lake. Considering every square of 1 unit area, the surface
	 * area of the lake is defined as the number of squares comprised by the
	 * lake.
	 * 
	 * @param grid
	 *            - the "z" values of all points in a grid.
	 * @param row
	 *            - the number of rows in grid
	 * @param col
	 *            - the number of columns in grid
	 * @param threshold
	 *            - the threshold value
	 * @return maximum surface area
	 * @see
	 */
	public int getMaximumSurfaceArea(int grid[][], int row, int col, int threshold) {

		List<List<Point>> allLakes = getAllLakes(grid, row, col, threshold);

		int maximumSurfaceArea = 0;

		for (int i = 0; i < allLakes.size(); i++) {
			maximumSurfaceArea = Math.max(maximumSurfaceArea, allLakes.get(i).size());
		}

		return maximumSurfaceArea;
	}

	/**
	 * 
	 * The method <code>getMaximumVolume</code> returns the maximum volume of
	 * the lake. Volume of the lake is defined as the sum of volumes of all
	 * squares comprised by the lake.
	 * 
	 * @param grid
	 *            - the "z" values of all points in a grid.
	 * @param row
	 *            - the number of rows in grid
	 * @param col
	 *            - the number of columns in grid
	 * @param threshold
	 *            - the threshold value
	 * @return maximum volume
	 * @see
	 */

	public int getMaximumVolume(int grid[][], int row, int col, int threshold) {

		List<List<Point>> allLakes = getAllLakes(grid, row, col, threshold);

		int maximumVolume = 0;

		for (int i = 0; i < allLakes.size(); i++) {

			int volumeOfLake = 0;
			for (int j = 0; j < allLakes.get(i).size(); j++) {
				int xCoordinate = allLakes.get(i).get(j).getX();
				int yCoordinate = allLakes.get(i).get(j).getY();

				volumeOfLake += grid[xCoordinate][yCoordinate];
			}

			maximumVolume = Math.max(maximumVolume, volumeOfLake);
		}

		return maximumVolume;

	}

	/**
	 * 
	 * The method <code>getShortestMotorablePath</code> returns the shortest
	 * motorable path between source and destination. A motorable path is
	 * defined as a path where, between any two consecutive points, the gradient
	 * should be less than a given threshold . Gradient between 2 adjacent
	 * points is defined as the absolute difference between their z values.
	 * 
	 * @param grid
	 *            - the "z" values of all points in a grid.
	 * @param row
	 *            - the number of rows in grid
	 * @param col
	 *            - the number of columns in grid
	 * @param source
	 *            - Point from which shortest motorable path is needed.
	 * @param destination
	 *            - Point to which shortest motorable path is needed.
	 * @param threshold
	 *            - the threshold value
	 * @return a list of all points that are part of the shortest motorable path
	 *         from source to destination in the required order.
	 * @see
	 */

	public List<Point> getShortestMotorablePath(int grid[][], int row, int col, Point source, Point destination,
			int threshold) {

		validateParams(grid, row, col, threshold);

		if (source.getX() < 0 || source.getX() >= row || source.getY() < 0 || source.getY() >= col) {
			throw new InvalidDataException("Source point is outside the grid");
		}

		if (destination.getX() < 0 || destination.getX() >= row || destination.getY() < 0
				|| destination.getY() >= col) {
			throw new InvalidDataException("Destination point is outside the grid");
		}

		int distance[][] = new int[row][col];
		Point parent[][] = new Point[row][col];
		boolean visited[][] = new boolean[row][col]; // initialised to false.

		// BFS can be used to find the shortest path in case when all the nodes
		// have same weight.
		Queue<Point> queue = new LinkedList<>();

		queue.add(source);
		distance[source.getX()][source.getY()] = 0;
		parent[source.getX()][source.getY()] = null;
		visited[source.getX()][source.getY()] = true;

		while (true) {

			Point point = queue.poll();

			// If queue becomes empty, return a null list as it means no path
			// exists.
			if (point == null) {
				return null;
			}

			int gridX = point.getX();
			int gridY = point.getY();

			// If we reach destination, we break.
			if (gridX == destination.getX() && gridY == destination.getY()) {
				break;
			}

			// Push the upward point to the queue if it is not visited and the
			// gradient is less than threshold
			if (gridX - 1 >= 0 && !visited[gridX - 1][gridY]
					&& Math.abs(grid[gridX - 1][gridY] - grid[gridX][gridY]) < threshold) {

				queue.add(new Point(gridX - 1, gridY));
				distance[gridX - 1][gridY] = distance[gridX][gridY] + 1;
				parent[gridX - 1][gridY] = point;
				visited[gridX - 1][gridY] = true;
			}

			// Push the downward point to the queue if it is not visited and the
			// gradient is less than threshold
			if (gridX + 1 < row && !visited[gridX + 1][gridY]
					&& Math.abs(grid[gridX + 1][gridY] - grid[gridX][gridY]) < threshold) {
				queue.add(new Point(gridX + 1, gridY));
				distance[gridX + 1][gridY] = distance[gridX][gridY] + 1;
				parent[gridX + 1][gridY] = point;
				visited[gridX + 1][gridY] = true;
			}

			// Push the left point to the queue if it is not visited and the
			// gradient is less than threshold
			if (gridY - 1 >= 0 && !visited[gridX][gridY - 1]
					&& Math.abs(grid[gridX][gridY - 1] - grid[gridX][gridY]) < threshold) {

				queue.add(new Point(gridX, gridY - 1));
				distance[gridX][gridY - 1] = distance[gridX][gridY] + 1;
				parent[gridX][gridY - 1] = point;
				visited[gridX][gridY - 1] = true;

			}

			// Push the right point to the queue if it is not visited and the
			// gradient is less than threshold
			if (gridY + 1 < col && !visited[gridX][gridY + 1]
					&& Math.abs(grid[gridX][gridY + 1] - grid[gridX][gridY]) < threshold) {
				queue.add(new Point(gridX, gridY + 1));
				distance[gridX][gridY + 1] = distance[gridX][gridY] + 1;
				parent[gridX][gridY + 1] = point;
				visited[gridX][gridY + 1] = true;
			}

		}

		return getPath(parent, source, destination);

	}

	/**
	 * The method <code>validateParams</code> validates the parameters. Throws
	 * InvalidDataException if parameter validation fails. Numbers of rows,
	 * columns have to be positive. Threshold can't be negative. The z values
	 * can't be negative because the water level can't be negative.
	 * 
	 * @param grid
	 *            - the "z" values of all points in a grid.
	 * @param row
	 *            - the number of rows in grid
	 * @param col
	 *            - the number of columns in grid
	 * @param threshold
	 *            - the threshold value
	 * @see
	 */
	private void validateParams(int[][] grid, int row, int col, int threshold) {

		if (row <= 0) {
			throw new InvalidDataException("Row has to be positive");
		}

		if (col <= 0) {
			throw new InvalidDataException("Column has to be negative");
		}

		if (threshold < 0) {
			throw new InvalidDataException("Threshold can't be negative");
		}

		for (int i = 0; i < row; i++) {
			for (int j = 0; j < col; j++) {
				if (grid[i][j] < 0) {
					throw new InvalidDataException("z value for any cell in the grid can't be negative");
				}
			}
		}

	}

	/**
	 * The method <code>dfsVisit</code> visits all the possible adjacent
	 * neighbors using DFS and adds all the visited points to lake.
	 * 
	 * @param grid
	 *            - the "z" values of all points in a grid.
	 * @param row
	 *            - the number of rows in grid
	 * @param col
	 *            - the number of columns in grid
	 * @param threshold
	 *            - the threshold value
	 * @param visited
	 *            - the visited matrix to check if a point is visited or not.
	 * @param rowIndex
	 *            - the rowIndex on which we are working currently.
	 * @param colIndex
	 *            - the colIndex on which we are working currently.
	 * @param lake
	 *            - the list of points constituting a lake.
	 * @see
	 */
	private void dfsVisit(int[][] grid, int row, int col, int threshold, boolean[][] visited, int rowIndex,
			int colIndex, List<Point> lake) {

		visited[rowIndex][colIndex] = true;
		lake.add(new Point(rowIndex, colIndex));

		// Move up if it is a valid move
		if (rowIndex - 1 >= 0 && !visited[rowIndex - 1][colIndex] && grid[rowIndex - 1][colIndex] < threshold) {
			dfsVisit(grid, row, col, threshold, visited, rowIndex - 1, colIndex, lake);
		}

		// Move down if it is a valid move
		if (rowIndex + 1 < row && !visited[rowIndex + 1][colIndex] && grid[rowIndex + 1][colIndex] < threshold) {
			dfsVisit(grid, row, col, threshold, visited, rowIndex + 1, colIndex, lake);
		}

		// Move left if it is a valid move
		if (colIndex - 1 >= 0 && !visited[rowIndex][colIndex - 1] && grid[rowIndex][colIndex - 1] < threshold) {
			dfsVisit(grid, row, col, threshold, visited, rowIndex, colIndex - 1, lake);
		}

		// Move right if it is a valid move
		if (colIndex + 1 < col && !visited[rowIndex][colIndex + 1] && grid[rowIndex][colIndex + 1] < threshold) {
			dfsVisit(grid, row, col, threshold, visited, rowIndex, colIndex + 1, lake);
		}

	}

	/**
	 * The method <code>getPath</code> gets the path for a given parent matrix.
	 * 
	 * @param parent
	 *            - the matrix where parent[i][j] tells the predecessor of point
	 *            (i,j).
	 * @param source
	 *            - the source point
	 * @param destination
	 *            - the destination point
	 * @return List<Point> - the ordered path
	 * @see
	 */
	private List<Point> getPath(final Point[][] parent, final Point source, final Point destination) {

		List<Point> path = new ArrayList<Point>();

		Point point = destination;
		path.add(destination);
		while (!point.equals(source)) {

			point = parent[point.getX()][point.getY()];
			path.add(point);

		}

		Collections.reverse(path);

		return path;

	}

}
